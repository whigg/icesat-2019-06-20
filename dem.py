import os
import numpy as np
import pyproj
import rasterio
import geojson
from geoid import GeoidHeight
import subset

geoid = GeoidHeight('egm2008-5.pgm')
with open('larsen.geojson', 'r') as geojson_file:
    larsen_geometry = geojson.load(geojson_file)
polygon = subset.polygonize(larsen_geometry)
bounds = np.array(polygon.bounds)
bounds[0:2] -= 5e3
bounds[2:4] += 5e3

with rasterio.open('REMA_1km_dem.tif', 'r') as dataset:
    window = rasterio.windows.from_bounds(*bounds, dataset.transform,
                                          dataset.height, dataset.width)
    transform = rasterio.windows.transform(window, dataset.transform)
    profile = dataset.profile.copy()
    profile.update(height=window.height, width=window.width,
                   transform=transform, driver='GTiff')
    data = dataset.read(indexes=1, window=window, masked=True)

with rasterio.open('larsen.tif', 'w', **profile) as dataset:
    dataset.write(data, indexes=1)

transformer = pyproj.Transformer.from_crs('epsg:3031', 'epsg:4326')
with rasterio.open('larsen.tif', 'r') as dataset:
    bounds = dataset.bounds
    profile = dataset.profile.copy()

x = np.linspace(bounds.left, bounds.right, dataset.width)
y = np.linspace(bounds.top, bounds.bottom, dataset.height)

ny, nx = dataset.shape
height = np.zeros((ny, nx), dtype=np.float32)
for i in range(ny):
    for j in range(nx):
        lat, lon = transformer.transform(x[j], y[i])
        height[i, j] = geoid.get(lat, lon)

with rasterio.open('geoid.tif', 'w', **profile) as output:
    output.write(height, indexes=1)
